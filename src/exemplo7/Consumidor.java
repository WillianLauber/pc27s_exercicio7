/**
 * Consumidor
 * Exemplo com Múltiplas Threads
 * 
 * Autor: Willian Lauber
 * Ultima modificacao: 16/08/2017
 */
package exemplo7;

import java.util.Random;

import java.util.Random;

public class Consumidor implements Runnable {

    private final static Random generator = new Random();
    private final Buffer[] buffer;

    private int indice;

    public Consumidor(Buffer[] shared, int indice) {
        buffer = shared;
        this.indice = indice;
    }

    @Override
    public void run() {
        int[] sum = new int[buffer.length];

        for (int count = 1; count <= 10; count++) {

            try {
                //Dorme, adquire um valor do Buffer e soma ele (nao atribui nada no Buffer compartilhado)
                Thread.sleep(generator.nextInt(300));

                sum[this.indice] += buffer[this.indice].get();
                System.out.printf("SomaFilosofo:  %2d\n", sum[this.indice]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("\nFilosofo comeu totalizando: " + sum[indice] + " O filosofo morreu");
    }

}
