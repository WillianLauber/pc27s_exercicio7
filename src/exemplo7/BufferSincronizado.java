/**
 * Exemplo de BufferSincronizado com métodos
 * wait() e notifyAll()
 * 
 * Autor: Willian Lauber
 * Ultima modificacao: 19/10/2017
 */
package exemplo7;

/**
 *
 * @author Lucio
 */
public class BufferSincronizado implements Buffer {

    private int buffer = -1; // compartilhado por produtor e consumidor
    private boolean temValor = false; //indica se o buffer temValor
    
    //Insere o valor no buffer
    public synchronized void set(int valor) throws InterruptedException{
        
        //Enquanto nao houver posicoes vazias, coloca a thread em estado de Espera
        while ( temValor ){
            System.out.println("Filosofo tenta comer.");
            System.out.println("Mesa cheia. Filosofo espera. | Buffer: " + buffer + " temGarfo: " + temValor);
            wait();
        }
        
        buffer = valor; //insere um valor no buffer
        
        //Indica que o produtor não pode armazenar outro valor ate que
        //o consumidor recupere o valor do buffer.
        temValor = true; 
        
        System.out.println("Filosofo come : " + buffer+ " | Total de comida: " + buffer + " temGarfo: " + temValor);
        
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        notifyAll(); 
    }
    
    public synchronized int get() throws InterruptedException{
    
        while (!temValor){
            System.out.println("Filosofo tenta comer.");
            System.out.println("Buffer vazio. Filosofo espera. | Buffer: " + buffer + " garfoLivre: " + temValor);
            wait();
        }
        
        //Consumidor acabou de recuperar valor do buffer.
        //Produtor pode inserir outro valor
        temValor=false;
        
        System.out.println("Filosofo come: " + buffer + " | Buffer: " + buffer + " garfoLivre: " + temValor);
        
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        notifyAll();
        
        return buffer;
        
    }//fim get
    
}//fim classe
