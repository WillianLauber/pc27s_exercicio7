/**
 * Buffer
 * 
 * Autor: Willian Lauber
 * Ultima modificacao: 19/10/2017
 */
package exemplo7;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
